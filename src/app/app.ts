import IModule = angular.IModule
import IScope = angular.IScope
import IAttributes = angular.IAttributes;

import { FieldModule } from './field/field'
import { PieceModule } from './piece/piece'
import { FieldService } from './field/field.service'
import { IPiece } from './piece/piece.interface'
import { IField } from './field/field.interface'
import { AppComponent } from './app.component'
import { generateAllMoves } from './pattern/generate.moves'
import { generateAllAttacks } from './pattern/generate.attacks'

function numberFromCharCode(code: number): number {
  switch (code) {
    case 48: return 0;
    case 49: return 1;
    case 50: return 2;
    case 51: return 3;
    case 52: return 4;
    case 53: return 5;
    case 54: return 6;
    case 55: return 7;
    case 56: return 8;
    case 57: return 9;
    default: break;
  }
}

interface PieceScope extends IScope {
  piece: IPiece,
  field: IField,
  moveStatus: string,
}

let isMove: boolean // stan 'w ruchu'/'nie w ruchu'
let pieceToMove: IPiece // obiekt bierki do przeniesienia
let targetId: any = { current: { number: -1, string: '' }, last: { number: -1, string: '' } } // id kliknietego pola
let targetPiece: IPiece
let availableMoves: number[] // tablica mozliwych ruchow
let availableAttacks: number[] //tablica mozliwych atakow

// dyrektywa do ruchu
function putPieceDirective(FieldService: any): any {

  'ngInject'
  return {
    scope: {
      field: '=',
      moveStatus: '=',
      piece: '=',
    },
    restrict: 'AE',
    link: function(scope: PieceScope, element: JQuery, attr: IAttributes): any {

      // dodajemy listener - jeśli uzytkownik kliknie w pole z figurą, zostanie rozpoczęta procedura ruchu
      angular.element(element).on('click', function(event: any): any {
        targetId.current.string = (numberFromCharCode(event.target.id.charCodeAt(5))) + '' + (numberFromCharCode(event.target.id.charCodeAt(6)));
        targetId.current.number = Number(targetId.current.string);
        targetPiece = FieldService.findFieldById(targetId.current.number).piece;
        // jeśli jesteśmy w trakcie ruchu (tzn. 'podnieśliśmy' figure)
        if (isMove) {

          let exceptionalMove: boolean = false;
          // sprawdzamy czy ruch istnieje w tablicy mozliwych ruchow
          if ((availableMoves.indexOf(targetId.current.number) < 0) && (availableAttacks.indexOf(targetId.current.number) < 0)) {
            console.log('ERROR: Ruch niemozliwy.')
            return;
          }
          // sprawdzamy czy pole jest zajete
          if (!FieldService.isFieldEmpty(targetId.current.number, pieceToMove)) {
            // sprawdzamy czy jest zajęte przez przyjazną bierkę
            if (targetPiece.color === pieceToMove.color) {
              if (!FieldService.isCastling(targetId.current.number, targetId.last.number)) {
                console.log('ERROR: To pole jest zajęte przez Twoją bierkę.');
                return;
              } else if (!targetPiece.moved) { // robimy roszadę
                console.log('INFO: Roszada.');
                if (targetId.last.number < targetId.current.number) {
                  FieldService.setPieceOnFieldById(targetId.current.number - 2, scope.piece);
                  FieldService.setPieceOnFieldById(targetId.current.number - 1, pieceToMove);
                  FieldService.setPieceOnFieldById(targetId.last.number, FieldService.nullPiece);
                  FieldService.setPieceOnFieldById(targetId.current.number, FieldService.nullPiece);
                } else {
                  FieldService.setPieceOnFieldById(targetId.current.number + 2, scope.piece);
                  FieldService.setPieceOnFieldById(targetId.current.number + 1, pieceToMove);
                  FieldService.setPieceOnFieldById(targetId.last.number, FieldService.nullPiece);
                  FieldService.setPieceOnFieldById(targetId.current.number, FieldService.nullPiece);
                }
                exceptionalMove = true;
              } else {
                console.log('ERROR: Roszada niemozliwa.');
                return;
              }
            }
          }

          // jesli to byl zwykly ruch
          if (!exceptionalMove) {
            // wszystko spoko -> wykonujemy ruch
            FieldService.setPieceOnFieldById(targetId.current.number, pieceToMove);
            FieldService.setPieceOnFieldById(targetId.last.number, FieldService.nullPiece);
          }
          // jeśli to byl ruch pionka, zmiejszamy mu limit do 1
          if (pieceToMove.name === 'Pawn') {
            pieceToMove.pattern.limit = 1;
          }
          // konczymy wyświetlanie uzytkownikowi wszystkich mozliwych ruchow
          availableMoves.forEach(function(item: number, index: number): any {
            if (document.getElementById('field' + item.toString())) {
              document.getElementById('field' + item.toString()).setAttribute('class', 'field');
            }
          })
          availableAttacks.forEach(function(item: number, index: number): any {
            if (document.getElementById('field' + item.toString())) {
              document.getElementById('field' + item.toString()).setAttribute('class', 'field');
            }
          })
          // po wykonaniu ruchu zmieniamy stan na 'nie w ruchu'
          isMove = false;
          // ... zapisujemy ze bierka wykonala ruchu ...
          pieceToMove.moved = true;
          targetPiece.moved = true;
          // ... i zmieniamy ruszającego się gracza
          if (targetPiece.name === 'King' && !exceptionalMove) {
            scope.moveStatus = 'mat' + targetPiece.color;
          } else {
            scope.moveStatus = (scope.moveStatus === 'white') ? 'black' : 'white';
            FieldService.changeMove();
          }

          //  jeśli nie jesteśmy w trakcie ruchu to przechodzimy w stan ruchu
        } else {
          // sprawdzamy czy nie kliknieto w puste pole
          if (!scope.piece || scope.piece === FieldService.nullPiece) {
            console.log('ERROR: Nic tu nie ma.');
            return;
          }
          // sprawdzamy czy to jest ruch dla danego koloru
          if (FieldService.whoMoves !== scope.piece.color) {
            console.log('ERROR: Nie Twoj ruch.');
            return;
          }
          pieceToMove = angular.copy(scope.piece)
          // document.getElementById('field' + targetId.current.string).setAttribute('class', 'field selected')
          targetId.last.string = targetId.current.string;
          targetId.last.number = targetId.current.number;
          // generujemy wszystkie mozliwe ruchy i ataki
          availableMoves = generateAllMoves(FieldService, targetId.current.number, pieceToMove.pattern);
          availableAttacks = generateAllAttacks(FieldService, targetId.current.number, pieceToMove.attackPattern)
          // jeśli nie wygenerowano ruchow, wychodzimy
          if (!availableMoves.length && !availableAttacks.length) {
            return;
          }
          // wyswietlamy je uzytkownikowi
          availableMoves.forEach(function(item: number, index: number): any {
            document.getElementById('field' + item.toString()).setAttribute('class', 'field selected');
          });
          availableAttacks.forEach(function(item: number, index: number): any {
            document.getElementById('field' + item.toString()).setAttribute('class', 'field selected attackable');
          });
          // ustawiamy stan na 'w ruchu'
          isMove = true;
        }
        // niestety niezbędne
        scope.$apply();
      });

      // umozliwiamy uzytkownikowi anulowanie ruchu
      document.getElementById('cancelmove').addEventListener('click', function(): any {
        isMove = false;
        let elementsClassSelected: any = document.getElementsByClassName('field selected');
        for (let i: number = 0; i < elementsClassSelected.length; i++) {
          elementsClassSelected[i].setAttribute('class', 'field');
        }
      });
    },
  }
}


export const ComponentsModule: IModule = angular
  .module('app.components', [
    FieldModule.name,
    PieceModule.name,
  ])
  .component('app', AppComponent)
  .config(($locationProvider: any) => {
    $locationProvider.html5Mode(true)
  })
  .directive('putPiece', putPieceDirective)
