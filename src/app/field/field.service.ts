import { IField } from './field.interface'
import { IPiece } from '../piece/piece.interface'
import { Pieces } from '../piece/pieces'

export class FieldService {

    public nullPiece: IPiece = {
        color: undefined,
        img: 'img/undefined.png',
        moved: undefined,
        name: undefined,
        pattern: undefined,
        attackPattern: undefined,
    }

    public whoMoves: string = 'white';

    public changeMove(): void {
        this.whoMoves = (this.whoMoves === 'white') ? 'black' : 'white';
    }


    public isCastling(currentId: number, lastId: number): boolean {
        if (this.findFieldById(currentId).piece.name === 'King' && this.findFieldById(lastId).piece.name === 'Rook') {
            return true;
        } else {
            return false;
        }
    }

    public fields: IField[][] = [
        // rząd 1
        [
            { id: 11, piece: Pieces.rookBlack },
            { id: 12, piece: Pieces.knightBlack },
            { id: 13, piece: Pieces.bishopBlack },
            { id: 14, piece: Pieces.kingBlack },
            { id: 15, piece: Pieces.queenBlack },
            { id: 16, piece: Pieces.bishopBlack },
            { id: 17, piece: Pieces.knightBlack },
            { id: 18, piece: Pieces.rookBlack },
        ],
        // rząd 2
        [
            { id: 21, piece: Pieces.pawnBlack },
            { id: 22, piece: Pieces.pawnBlack },
            { id: 23, piece: Pieces.pawnBlack },
            { id: 24, piece: Pieces.pawnBlack },
            { id: 25, piece: Pieces.pawnBlack },
            { id: 26, piece: Pieces.pawnBlack },
            { id: 27, piece: Pieces.pawnBlack },
            { id: 28, piece: Pieces.pawnBlack },
        ],
        // rząd 3
        [
            { id: 31, piece: this.nullPiece },
            { id: 32, piece: this.nullPiece },
            { id: 33, piece: this.nullPiece },
            { id: 34, piece: this.nullPiece },
            { id: 35, piece: this.nullPiece },
            { id: 36, piece: this.nullPiece },
            { id: 37, piece: this.nullPiece },
            { id: 38, piece: this.nullPiece },
        ],
        // rząd 4
        [
            { id: 41, piece: this.nullPiece },
            { id: 42, piece: this.nullPiece },
            { id: 43, piece: this.nullPiece },
            { id: 44, piece: this.nullPiece },
            { id: 45, piece: this.nullPiece },
            { id: 46, piece: this.nullPiece },
            { id: 47, piece: this.nullPiece },
            { id: 48, piece: this.nullPiece },
        ],
        // rząd 5
        [
            { id: 51, piece: this.nullPiece },
            { id: 52, piece: this.nullPiece },
            { id: 53, piece: this.nullPiece },
            { id: 54, piece: this.nullPiece },
            { id: 55, piece: this.nullPiece },
            { id: 56, piece: this.nullPiece },
            { id: 57, piece: this.nullPiece },
            { id: 58, piece: this.nullPiece },
        ],
        // rząd 6
        [
            { id: 61, piece: this.nullPiece },
            { id: 62, piece: this.nullPiece },
            { id: 63, piece: this.nullPiece },
            { id: 64, piece: this.nullPiece },
            { id: 65, piece: this.nullPiece },
            { id: 66, piece: this.nullPiece },
            { id: 67, piece: this.nullPiece },
            { id: 68, piece: this.nullPiece },
        ],
        // rząd 7
        [
            { id: 71, piece: Pieces.pawnWhite },
            { id: 72, piece: Pieces.pawnWhite },
            { id: 73, piece: Pieces.pawnWhite },
            { id: 74, piece: Pieces.pawnWhite },
            { id: 75, piece: Pieces.pawnWhite },
            { id: 76, piece: Pieces.pawnWhite },
            { id: 77, piece: Pieces.pawnWhite },
            { id: 78, piece: Pieces.pawnWhite },
        ],
        // rząd 8
        [
            { id: 81, piece: Pieces.rookWhite },
            { id: 82, piece: Pieces.knightWhite },
            { id: 83, piece: Pieces.bishopWhite },
            { id: 84, piece: Pieces.kingWhite },
            { id: 85, piece: Pieces.queenWhite },
            { id: 86, piece: Pieces.bishopWhite },
            { id: 87, piece: Pieces.knightWhite },
            { id: 88, piece: Pieces.rookWhite },
        ],
    ];

    public findFieldById(id: number): IField {
        for (let i: number = 0; i < 8; i++) {
            for (let j: number = 0; j < 8; j++) {
                if (this.fields[i][j].id === id) {
                    return this.fields[i][j];
                }
            }
        }
    }
    public setPieceOnFieldById(id: number, piece: IPiece): void {
        this.findFieldById(id).piece = piece;
    }

    public isFieldEmpty(id: number, piece: IPiece): boolean {
        if (!this.isItEmpty(id)) { return false }
        return true;
    }

    public isItEmpty(id: number): boolean {
        return this.findFieldById(id).piece.img === 'img/undefined.png'
    }
}
