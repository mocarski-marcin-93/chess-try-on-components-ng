import { IPiece } from '../piece/piece.interface'

export interface IField {
    id: number,
    piece: IPiece,
}
