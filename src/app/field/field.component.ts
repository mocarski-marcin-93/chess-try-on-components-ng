import { IField } from './field.interface'
import { FieldService } from './field.service'
import { IScope } from 'angular'
import IComponentOptions = angular.IComponentOptions;

export const FieldComponent: IComponentOptions = {

  controller: class {
    public fields: IField[][];
    public moveStatus: string;

    constructor(FieldService: any, $scope: IScope) {
      'ngInject';

      this.fields = FieldService.fields;
      this.moveStatus = FieldService.whoMoves;
    }

    public checkMat(): Object {
      let finish: Object = { isFinish: false, winner: '' };
      if (this.moveStatus.indexOf('mat') > -1) {
        if (this.moveStatus.indexOf('white') > -1) {
          finish = { isFinish: true, winner: 'Black' };
        } else {
          finish = { isFinish: true, winner: 'White' };
        }
      }
      return finish;
    }
  },

  template: `
    <div class='row' ng-repeat='row in $ctrl.fields'>
      <div class='field' ng-repeat='field in row' put-piece piece='field.piece' field='field' move-status='$ctrl.moveStatus' id='field{{ field.id }}'>
      {{ field.id }} <img ng-src='{{ field.piece.img }}' id='imgOn{{ field.id }}'>
      </div>
    </div>
    <div class='whomoves {{ $ctrl.moveStatus }}' > Move: {{ $ctrl.moveStatus }} </div>
    <div class='mat' ng-show='$ctrl.checkMat().isFinish'> {{ $ctrl.checkMat().winner }} wins! </div>
  `,
}
