import IModule = angular.IModule
// import { IAttributes } from 'angular'
// import { IScope } from 'angular'
import { FieldComponent } from './field.component'
import { FieldService } from './field.service'
// import { IField } from './field.interface';

// interface PieceScope extends IScope {
//     field: IField,
// }
//
// function putImgSrc(FieldService: any) {
//     'ngInject'
//     return {
//         replace: true,
//         scope: {
//             field: '=',
//         },
//         restrict: 'AE',
//         link: function(scope: PieceScope, element: JQuery, attr: IAttributes) {
//
//             if (scope.field.piece) element.attr('src', scope.field.piece.img);
//             FieldService.setPieceOnFieldById(scope.field.id, scope.field.piece);
//             if (!scope.$digest) scope.$apply();
//         }
//     }
// }

export const FieldModule: IModule = angular
    .module('app.component.field', [])
    .service('FieldService', FieldService)
    .component('field', FieldComponent)
    // .directive('imgSrc', putImgSrc)
