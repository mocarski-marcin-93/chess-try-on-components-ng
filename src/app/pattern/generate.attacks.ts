import { IPattern } from './pattern.interface'
import { FieldService } from '../field/field.service'

export function generateAllAttacks(FieldService: any, fieldId: number, pattern: IPattern): number[] {

  'ngInject'
  let allMoves: number[] = [];
  let newMove: number;
  let newMoveX: number;
  let newMoveY: number;
  let inBoard: boolean;

  for (let i: number = 0; i < pattern.vectors.length; i++) {
    for (let j: number = 1; j <= pattern.limit; j++) {
      newMoveX = Math.floor(fieldId / 10) + j * pattern.vectors[i].x;
      newMoveY = fieldId % 10 + j * pattern.vectors[i].y;
      inBoard = true;

      if (newMoveX > 0 && newMoveX < 9 && newMoveY > 0 && newMoveY < 9) {
        newMove = newMoveX * 10 + newMoveY;
      } else {
        inBoard = false;
      }
      if (inBoard) {
        if ((FieldService.findFieldById(newMove).piece !== FieldService.nullPiece)) {
          if (FieldService.findFieldById(fieldId).piece.color !== FieldService.findFieldById(newMove).piece.color) {
            allMoves.push(newMove);
          }
          break;
        }
      }
    }
  }
  return allMoves;
}
