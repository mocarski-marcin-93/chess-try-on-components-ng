interface IVector {
  x: number,
  y: number
}

export interface IPattern {
    vectors: IVector[],
    limit: number
}
