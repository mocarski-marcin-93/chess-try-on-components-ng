import { IPattern } from './pattern.interface'
import { FieldService } from '../field/field.service'

export function generateAllMoves(FieldService: any, fieldId: number, pattern: IPattern): number[] {
  'ngInject'
  let allMoves: number[] = [];
  let newMove: number;
  let newMoveX: number;
  let newMoveY: number;
  let currentPiece = FieldService.findFieldById(fieldId).piece;
  let targetPiece: any;

  for (let i: number = 0; i < pattern.vectors.length; i++) {
    for (let j: number = 1; j <= pattern.limit; j++) {
      newMoveX = Math.floor(fieldId / 10) + j * pattern.vectors[i].x;
      newMoveY = fieldId % 10 + j * pattern.vectors[i].y;

      if (newMoveX > 0 && newMoveX < 9 && newMoveY > 0 && newMoveY < 9) {
        newMove = newMoveX * 10 + newMoveY;
        targetPiece = FieldService.findFieldById(newMove).piece;
      } else {
        continue;
      }
      if (targetPiece !== FieldService.nullPiece) {
        if (targetPiece.name === 'King' && !targetPiece.moved && currentPiece.name === 'Rook') {
          allMoves.push(newMove);
        }
        break;
      }
      if (newMove > 10 && newMove < 89 && allMoves.indexOf(newMove) < 0) {
        allMoves.push(newMove);
      }
    }
  }
  return allMoves;
}
