import { IPattern } from '../pattern/pattern.interface'

export interface IPiece {
    img: string,
    name: string,
    color: string,
    moved: boolean,
    pattern: IPattern,
    attackPattern: IPattern,
}
