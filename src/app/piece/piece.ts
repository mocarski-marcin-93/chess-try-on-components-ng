import IModule = angular.IModule
import { PieceComponent } from './piece.component'

export const PieceModule: IModule = angular
    .module('app.component.piece', [])
    .component('piece', PieceComponent)
