export var Pieces: any = {
  pawnWhite: {
    color: 'white',
    img: 'img/piece.white.pawn.png',
    moved: false,
    name: 'Pawn',
    pattern: {
      limit: 2,
      vectors: [{ x: -1, y: 0 }],
    },
    attackPattern: {
      limit: 1,
      vectors: [{ x: -1, y: 1 }, { x: -1, y: -1 }],
    },
  },
  rookWhite: {
    color: 'white',
    img: 'img/piece.white.rook.png',
    moved: false,
    name: 'Rook',
    pattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 0 }, { x: 0, y: -1 }],
    },
    attackPattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 0 }, { x: 0, y: -1 }],
    },
  },
  knightWhite: {
    color: 'white',
    img: 'img/piece.white.knight.png',
    moved: false,
    name: 'Knight',
    pattern: {
      limit: 1,
      vectors: [{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 1, y: -2 }, { x: 2, y: -1 }, { x: -1, y: 2 }, { x: -2, y: 1 }, { x: -1, y: -2 }, { x: -2, y: -1 }],
    },
    attackPattern: {
      limit: 1,
      vectors: [{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 1, y: -2 }, { x: 2, y: -1 }, { x: -1, y: 2 }, { x: -2, y: 1 }, { x: -1, y: -2 }, { x: -2, y: -1 }],
    },
  },
  bishopWhite: {
    color: 'white',
    img: 'img/piece.white.bishop.png',
    moved: false,
    name: 'Bishop',
    pattern: {
      limit: 7,
      vectors: [{ x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 1 }, { x: -1, y: -1 }],
    },
    attackPattern: {
      limit: 7,
      vectors: [{ x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 1 }, { x: -1, y: -1 }],
    },
  },
  queenWhite: {
    color: 'white',
    img: 'img/piece.white.queen.png',
    moved: false,
    name: 'Queen',
    pattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
    attackPattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
  },
  kingWhite: {
    color: 'white',
    img: 'img/piece.white.king.png',
    moved: false,
    name: 'King',
    pattern: {
      limit: 1,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
    attackPattern: {
      limit: 1,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
  },

  pawnBlack: {
    color: 'black',
    img: 'img/piece.black.pawn.png',
    moved: false,
    name: 'Pawn',
    pattern: {
      limit: 2,
      vectors: [{ x: 1, y: 0 }],
    },
    attackPattern: {
      limit: 1,
      vectors: [{ x: 1, y: 1 }, { x: 1, y: -1 }],
    },
  },
  rookBlack: {
    color: 'black',
    img: 'img/piece.black.rook.png',
    moved: false,
    name: 'Rook',
    pattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 0 }, { x: 0, y: -1 }],
    },
    attackPattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 0 }, { x: 0, y: -1 }],
    },
  },
  knightBlack: {
    color: 'black',
    img: 'img/piece.black.knight.png',
    moved: false,
    name: 'Knight',
    pattern: {
      limit: 1,
      vectors: [{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 1, y: -2 }, { x: 2, y: -1 }, { x: -1, y: 2 }, { x: -2, y: 1 }, { x: -1, y: -2 }, { x: -2, y: -1 }],
    },
    attackPattern: {
      limit: 1,
      vectors: [{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 1, y: -2 }, { x: 2, y: -1 }, { x: -1, y: 2 }, { x: -2, y: 1 }, { x: -1, y: -2 }, { x: -2, y: -1 }],
    },
  },
  bishopBlack: {
    color: 'black',
    img: 'img/piece.black.bishop.png',
    moved: false,
    name: 'Bishop',
    pattern: {
      limit: 7,
      vectors: [{ x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 1 }, { x: -1, y: -1 }],
    },
    attackPattern: {
      limit: 7,
      vectors: [{ x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 1 }, { x: -1, y: -1 }],
    },
  },
  queenBlack: {
    color: 'black',
    img: 'img/piece.black.queen.png',
    moved: false,
    name: 'Queen',
    pattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
    attackPattern: {
      limit: 7,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
  },
  kingBlack: {
    color: 'black',
    img: 'img/piece.black.king.png',
    moved: false,
    name: 'King',
    pattern: {
      limit: 1,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
    attackPattern: {
      limit: 1,
      vectors: [{ x: 1, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }],
    },
  },
}
