import { IPiece } from './piece.interface'
import IComponentOptions = angular.IComponentOptions;

export const PieceComponent: IComponentOptions = {

    controller: class {
        public pieces: IPiece[];

        constructor() {
            return
        }
    },

    template: `
    <div>
      <h1> Piece component </h1>
    </div>
  `,
}
