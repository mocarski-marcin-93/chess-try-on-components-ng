const isProdEnv = process.env.WEBPACK_ENV === 'production';
const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const config = {
    devtool: 'source-map',
    // Na początek potrzebujemy plik wejściowy
    entry: './src/index',
    //3/ Oraz wynikowy
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'bundle.js',
    },
    module: {
        preLoaders: [{
            test: /\.ts$/,
            loaders: ['tslint'],
            exclude: /node_modules/
        }, ],
        loaders: [{
            test: /\.ts$/,
            loaders: ['ng-annotate', 'ts'],
            exclude: /node_modules/
        }, {
            test: /\.styl$/, // this is REGEXP
            loaders: ['style', 'css', 'stylus'] // kolejno ładuje loadery: stylus, css, style, zostały one wcześniej zainstalowane przez npm
        }, ],
    },
    resolve: {
        extensions: ['', '.js', '.ts', '.json']
    },
    plugins: isProdEnv ? [
        new webpack.optimize.UglifyJsPlugin(),
        new CopyWebpackPlugin([{
            from: './src/index.html',
            to: 'index.html'
        }])
    ] : [],
}

// var json = require("json!./file.json");
// // => returns file.json content as json parsed object

module.exports = config;
